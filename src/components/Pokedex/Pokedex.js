import React, { Component } from "react"
import ListPokedex from "./ListPokedex"
import './index.css'

class Pokedex extends Component {
  state = {
    check: false
  }

  listPokedex = (data) => {
    return data.map(val => {
        return <ListPokedex key={val.name} data={val} removePokemon={this.props.removePokemon}/>
    })
  }
  
  render() {
    console.log(this.props.pokedex)
    return (
      <div className="container">
        <div className="row mx-3 list-pokedex">
          {this.listPokedex(this.props.pokedex)}
        </div>
      </div>
    )
  }
}

export default Pokedex
