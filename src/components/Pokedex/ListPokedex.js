import React, { Component } from "react"



class ListPokedex extends Component {
  state = {
    check: false
  }


  displayLevel = level => {
    let result = []
    for (let i = 0; i < level; i++) {
      result.push(
        <img
          className="mr-2"
          src="images/cute.png"
          style={{ width: 30, height: 30 }}
        />
      )
    }
    return result
  }

  
  render() {
    const { data } = this.props
    return (
      <div className="col-sm-6 pokedex font p-3 mt-3">
        <div className="row">
        <div className="col-sm-4 ">
           <img className="img-pokedex" src={data.imageUrl}/>
        </div>
        <div className="col-sm-8">
              <div className="row">
                <div className="col-sm-12">
                  <div className="d-flex justify-content-between">
                  <h2>{data.name}</h2>
                  <h2 onClick={() => this.props.removePokemon(data)} style={{color : "#ec5656" , cursor : "pointer"}}>x</h2>
                </div>
                </div>
                <div className="col-sm-12">
                  <div className="row mt-2">
                    <div className="col-sm-3">HP</div>
                
                    <div className="col-sm-6">
                      <div className="tube" style={{ width: `${data.hp}%` }}>
                        &nbsp;
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-sm-12">
                  <div className="row mt-2">
                    <div className="col-sm-3">STR</div>
                    <div className="col-sm-6 ">
                      <div className="tube" style={{ width: "50%" }}>
                        &nbsp;
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-sm-12">
                  <div className="row mt-2">
                    <div className="col-sm-3">WEAK</div>
                    <div className="col-sm-6 ">
                      <div className="tube" style={{ width: `${data.weak}%` }}>
                        &nbsp;
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-sm-12 mt-2">
                  {this.displayLevel(data.level)}
                </div>
              </div>
            </div>
        </div>
       
      </div>
    )
  }
}

export default ListPokedex
