import React, { Component } from "react"
import {InputGroup , FormControl , Button} from "react-bootstrap"
import "./Search.css"

class Search extends Component {
  state = {
    check: false
  }
  render() {
      console.log(this.props)
    return (
      <InputGroup className="mb-3" style={{width : "38vw"}}>

        <FormControl
          aria-label="Default"
          aria-describedby="inputGroup-sizing-default"
          placeholder="Find Pokemon"
          className="font"
          value={this.props.search}
          onChange={this.props.searchPokemon}
        />
         <InputGroup.Append>
      <Button variant="outline-secondary"><img className="search" src="images/search.png"/></Button>
    </InputGroup.Append>
      </InputGroup>
    )
  }
}

export default Search
