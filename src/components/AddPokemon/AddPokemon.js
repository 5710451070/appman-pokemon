import React, { Component } from "react"
import InputGroup from "react-bootstrap/InputGroup"
import Modal from "react-bootstrap/Modal"
import Search from "../Search/Search"
import "./index.css"
import axios from "axios"

class AddPokemon extends Component {
  constructor(props) {
    super(props)
    this.state = {
      check: false,
      data: []
    }
  }


  renderCard = data => {
    return (
      data &&
      data.map(val => {
        return (
          <div className="row mx-1 py-3 list mb-3 font">
            <div className="col-sm-3 ">
              <img className="img-card" src={val.imageUrl} />
            </div>
            <div className="col-sm-9">
              <div className="row">
                <div className="col-sm-12">
                  <div className="d-flex justify-content-between">
                    <h2>{val.name}</h2>
                    <h2
                      style={{ color: "#ec5656", cursor: "pointer" }}
                      onClick={() => this.props.addPokemon(val)}
                    >
                      Add
                    </h2>
                  </div>
                </div>
                <div className="col-sm-12">
                  <div className="row mt-2">
                    <div className="col-sm-3">HP</div>
                    <div className="col-sm-6">
                      <div className="tube" style={{ width: `${val.hp}%` }}>
                        &nbsp;
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-sm-12">
                  <div className="row mt-2">
                    <div className="col-sm-3">STR</div>
                    <div className="col-sm-6 ">
                      <div className="tube" style={{ width: "50%" }}>
                        &nbsp;
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-sm-12">
                  <div className="row mt-2">
                    <div className="col-sm-3">WEAK</div>
                    <div className="col-sm-6 ">
                      <div className="tube" style={{ width: `${val.weak}%` }}>
                        &nbsp;
                      </div>
                    </div>
                  </div>
                </div>

                <div className="col-sm-12 mt-2">
                  {this.displayLevel(val.level)}
                </div>
              </div>
            </div>
          </div>
        )
      })
    )
  }

  displayLevel = level => {
    let result = []
    for (let i = 0; i < level; i++) {
      result.push(
        <img
          className="mr-2"
          src="images/cute.png"
          style={{ width: 30, height: 30 }}
        />
      )
    }
    return result
  }

  render() {
    return (
      <Modal
        size="lg"
        centered
        show={this.props.show}
        onHide={this.props.handleHide}
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title-lg"
      >
        <Modal.Header>
          <Modal.Title id="example-custom-modal-styling-title-lg">
            <Search
              searchPokemon={this.props.searchPokemon}
              search={this.props.search}
            />
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {this.renderCard(this.props.data)}
        </Modal.Body>
      </Modal>
    )
  }
}

export default AddPokemon
