import React, { Component } from "react"
import "./App.css"
import Footer from "./components/Footer/Footer"
import Search from "./components/Search/Search"
import Pokedex from "./components/Pokedex/Pokedex"
import axios from "axios"
const AddPokemon = React.lazy(() =>
  import("./components/AddPokemon/AddPokemon")
)

const COLORS = {
  Psychic: "#f8a5c2",
  Fighting: "#f0932b",
  Fairy: "#c44569",
  Normal: "#f6e58d",
  Grass: "#badc58",
  Metal: "#95afc0",
  Water: "#3dc1d3",
  Lightning: "#f9ca24",
  Darkness: "#574b90",
  Colorless: "#FFF",
  Fire: "#eb4d4b"
}

class App extends Component {
  state = {
    show: false,
    pokedex: [],
    data: [],
    search: ""
  }

  handleShow = () => {
    this.setState({ show: true })
  }

  handleHide = () => {
    this.setState({ show: false })
  }

  componentDidMount = async () => {
    const result = await axios.get("http://localhost:3030/api/cards")
    this.calculatePokemon(result.data.cards)
  }

  calculatePokemon = data => {
    data = data.map(val => {
      let hp = +val.hp > 100 ? 100 : +val.hp
      let atk = val.attacks ? val.attacks.length * 50 : 0
      let weak = val.weaknesses ? 100 : 0
      // let damage = val.attacks ? parseInt(val.attacks[0].damage ? val.attacks[0].damage : 0 , 10) : 0

      let damage = 0
      for (let i in val.attacks) {
        // console.log(val.attacks[i])
        let calculate = val.attacks
          ? parseInt(val.attacks[i].damage ? val.attacks[i].damage : 0, 10)
          : 0
        // console.log(calculate)
        damage = calculate + damage
      }

      let level = (hp / 10 + damage / 10 + 10 * (weak / 100)) / 5
      level = level ? Math.floor(level) : 0
      return {
        name: val.name,
        imageUrl: val.imageUrl,
        hp,
        atk,
        weak,
        damage,
        level
      }
    })
    console.log(data)
    this.setState({ data })

    // this.setState({ data: result.data.cards })
  }

  addPokemon = value => {
    let { data, pokedex } = this.state
    pokedex.push(value)
    const keyValue = [value].map(val => [val.name, value])
    const myMap = new Map(keyValue)
    const pokemons = data.filter(val => !myMap.has(val.name))
    this.setState({ data: pokemons, pokedex })
  }

  removePokemon = value => {
    let { data, pokedex } = this.state
    data.push(value)
    const keyValue = [value].map(val => [val.name, value])
    const myMap = new Map(keyValue)
    const pokedexs = pokedex.filter(val => !myMap.has(val.name))
    this.setState({ pokedex: pokedexs, data })
  }

  searchPokemon = async ({ target }) => {
    await this.setState({ search: target.value })
    const result = await axios.get(`http://localhost:3030/api/cards`)

    result.data.cards = result.data.cards.filter(
      val => val.name.includes(target.value) || val.type.includes(target.value)
    )
    // console.log(result.data.cards)
    this.calculatePokemon(result.data.cards)
  }

  render() {
    return (
      <div className="App">
        <div className="head">
          <h1>My Pokedex</h1>
        </div>
        <React.Suspense fallback={null}>
          <AddPokemon
            show={this.state.show}
            handleHide={this.handleHide}
            data={this.state.data}
            addPokemon={this.addPokemon}
            searchPokemon={this.searchPokemon}
            search={this.state.search}
          />
        </React.Suspense>

        <Pokedex
          pokedex={this.state.pokedex}
          removePokemon={this.removePokemon}
        />
        {/* <button onClick={() => this.setState({check : true})}>show</button>
          <button onClick={() => this.setState({check : false})}>hide</button>
          <div className={this.state.check ? `visible` : `hidden`}>dddddddddddddddddddddddddddddddddddddddd</div> */}
        <Footer handleShow={this.handleShow} />
      </div>
    )
  }
}

export default App
